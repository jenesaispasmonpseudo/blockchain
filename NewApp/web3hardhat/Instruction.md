## Démarrage du Projet
Utilisez npm start pour démarrer le projet 

## Vérification du Contrat
Un contrat est déjà ajouté dans le projet. Vous pouvez modifier votre adresse dans les fichiers contract-address.json et vous devez également changer l'ABI de votre contrat dans les fichiers ContractDetails.

## Déployer un novueau contrat

Vous devez vous rendre dans le dossier Mariage pour redeployer votre contrat de mariage.
Vous aurez besoins d'un compte Metamask avec sa clé privé et d'une clé api Inufura ainsi qu'uun clé api etherscan.
Une fois deployer vous devez le mint pour l'ajouter à votre portefeuile metamask.

## Guide d'Utilisation
Ceci est un test pour visualiser le contrat et utiliser les fonctions dans le contrat. Vous ne pouvez pas déployer de contrat.

Vous pouvez maintenat interagire avec votre contrat