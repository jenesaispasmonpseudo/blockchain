import React, { useState } from "react";
import "./Home.css";
import { ReadContract } from "./ReadContract";

export function Home({ contractAddress, contract, signer }) {
  const [goTo, setGoTo] = useState(false); 

  const handleSend = () => {
    setGoTo(true);
  };

  if (goTo) {
    return <ReadContract contractAddress={contractAddress} contract={contract} signer={signer} />;
  }

  return (
    <div>
      <header className="header">
        <p>Contrat Mariage&NFT</p>
      </header>
      <div className="sectionHome">
        <div className="containerPresentation">
          <h1>Optez pour les contrats de mariages avec NFT, plus simple, plus rapide, plus sécurisé</h1>
        </div>
        <div className="containerChoix">
          <button className="buttonChoix" onClick={handleSend}>Voir mon contrat</button>
          <button className="buttonChoix">Créer un nouveau contrat</button>
        </div>
      </div>
      <footer className="footer">
        <p>&copy; 2024 Tous droits réservés</p>
      </footer>
    </div>
  );
}
