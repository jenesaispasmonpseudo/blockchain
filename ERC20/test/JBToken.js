// scripts/deploy.js

const { ethers } = require("hardhat");
const { describe } = require("mocha");

describe("Deploying JBToken contract", function () {
  it("should deploy JBToken contract", async function () {
    const [deployer] = await ethers.getSigners();

    console.log("Deploying JBToken with the account:", deployer.address);

    const initialSupply = 1000000; 
    const JBToken = await ethers.getContractFactory("JBToken");
    const jbToken = await JBToken.deploy(initialSupply);

    console.log("JBToken deployed to:", jbToken.address);
  });
});
