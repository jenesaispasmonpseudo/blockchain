const { buildModule } = require("@nomicfoundation/hardhat-ignition/modules");

const TokenModule = buildModule("TokenModule", (m) => {
  // Définir l'approvisionnement initial que vous souhaitez
  const initialSupply = "1000000000000000000000000000"; 

  // Déployer le contrat JBToken en passant l'approvisionnement initial au constructeur
  const JBToken = m.contract("JBToken", [initialSupply]);

  return { JBToken };
});

module.exports = TokenModule;

// Pour indiquer a Hardhat de se connecter
// a un reseau specifique, il faut utiliser la commande