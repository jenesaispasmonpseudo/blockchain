const { buildModule } = require("@nomicfoundation/hardhat-ignition/modules");

const ExamModule = buildModule("ExamModule", (m) => {

  const student = 10;
  const studentMarks = 15;
  const Exam = m.contract("Exam",[10,10]);

  return { Exam };
});

module.exports = ExamModule;