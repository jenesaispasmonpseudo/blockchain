const { expect } = require("chai");
const { ethers } = require("hardhat");

//ether.js est une biblioteque qui permet d'intéragir avec les contrats

describe("Testing the contract", function() {
    it("should send the total to the owner", async function() {
        const[owner] = await ethers.getSigners();

        const Token = await ethers.deployContract("Token");

        const ownerBalance = await Token.balanceOf(owner.address);
        
        expect(await Token.totalSupply()).to.equal(ownerBalance);
        

    });
});