// SPDX-License-Identifier: SEE LICENSE IN LICENSE
pragma solidity ^0.8.24;

import "hardhat/console.sol";

contract Token  {
    string public name = "Token";
    string public symbole = "TKN";

    //Declare une variable de type uint
    uint256 public totalSupply= 1000000;

    //
    address public owner;

    mapping(address => uint) public balances;

    event Transfer(address indexed from, address indexed to, uint256 value);

    constructor() {
        balances[msg.sender] = totalSupply;
        owner = msg.sender;
    }

    function transfer(address to, uint256 value) external {
        require(balances[msg.sender] >= value, "Insufficient balance");
        
        balances[msg.sender] -=value;
        balances[to] += value;

        emit Transfer(msg.sender, to, value);

    }
    function balanceOf(address account) external view returns (uint256) {
        return balances[account];
    }
}

// pragma solidity ^0.8.0;

// import "hardhat/console.sol";

// import { ERC20 } from "@openzeppelin/contracts/token/ERC20/ERC20.sol";

// //La ligbe ci dessouus declare un contrat 
// contract Token {
//     string public name = "Token";
//     string public symbole = "TKN";

//     //Declare une variable de type uint
//     uint256 public totalSupply= 1000000;

//     //
//     address public owner;

//     mapping(address => uint) public balances;

//     event Transfer(address indexed from, address indexed to, uint256 value);

//     constructor() {
//         balances[msg.sender] = totalSupply;
//         owner = msg.sender;
//     }

//     function transfer(address to, uint256 value) external {
//         require(balances[msg.sender] >= value, "Insufficient balance");
        
//         balances[msg.sender] -=value;
//         balances[to] += value;

//         emit Transfer(msg.sender, to, value);

//     }
//     function balanceOf(address account) external view returns (uint256) {
//         return balances[account];
//     }
// }