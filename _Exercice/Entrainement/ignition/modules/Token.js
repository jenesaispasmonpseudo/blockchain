const { buildModule } = require("@nomicfoundation/hardhat-ignition/modules");

const TokenModule = buildModule("TokenModule", (m) => {
  // Définissez l'approvisionnement initial que vous souhaitez
  const initialSupply = 1000000;

  // Déployez le contrat GLDToken en passant l'approvisionnement initial au constructeur
  const GLDToken = m.contract("GLDToken", [initialSupply]);

  return { GLDToken };
});

module.exports = TokenModule;

// Pour indiquer a Hardhat de se connecter
// a un reseau specifique, il faut utiliser la commande