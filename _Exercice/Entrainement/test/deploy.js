// scripts/deploy.js

const { ethers } = require("hardhat");
const { describe } = require("mocha");

describe("Deploying GLDToken contract", function () {
  it("should deploy GLDToken contract", async function () {
    const [deployer] = await ethers.getSigners();

    console.log("Deploying GLDToken with the account:", deployer.address);

    const initialSupply = 1000000;
    const GLDToken = await ethers.getContractFactory("GLDToken");
    const gldToken = await GLDToken.deploy(initialSupply);

    console.log("GLDToken deployed to:", gldToken.address);
  });
});
