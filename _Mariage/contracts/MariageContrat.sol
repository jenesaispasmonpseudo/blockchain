// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract MarriageNFT is ERC721 {
    uint256 private tokenIdCounter;
    address private contractOwner;

    struct Child {
        string name;
        uint256 birthDate;
        string gender;
        string hairColor;
        string eyeColor;
        string nationality;
    }
    struct Marry {
        string name;
        uint256 birthDate;
        string gender;
        string hairColor;
        string eyeColor;
        string nationality;
        string name2;
        uint256 birthDate2;
        string gender2;
        string hairColor2;
        string eyeColor2;
        string nationality2;
    }

    mapping(uint256 => Child[]) private childrenRegistry;
    mapping(uint256 => Marry[]) private marriages;
    mapping(uint256 => address) private tokenOwners;

    constructor() ERC721("MarriageContract", "MC") {
        contractOwner = msg.sender;
    }

    function mint(address to) external {
        require(msg.sender == contractOwner, "Only contract owner can mint");
        _safeMint(to, tokenIdCounter);
        tokenOwners[tokenIdCounter] = to; // Stocker l'adresse du propriétaire du NFT
        tokenIdCounter++;

    }

    function totalSupply() external view returns (uint256) {
        return tokenIdCounter;
    }

    function getNextTokenId() public view returns (uint256) {
        return tokenIdCounter;
    }

    // Fonction pour ajouter un enfant à un NFT de mariage
    function addChild(uint256 tokenId, string memory childName, uint256 birthDate, string memory gender, string memory hairColor, string memory eyeColor, string memory nationality) external {
        require(msg.sender == contractOwner, "Only contract owner can add children");
        childrenRegistry[tokenId].push(Child(childName, birthDate , gender, hairColor, eyeColor, nationality));
    }

    function getChildren(uint256 tokenId) external view returns (Child[] memory) {
        return childrenRegistry[tokenId];
    }
    
   function addMarry(
    uint256 tokenId,
    string memory _name,
    uint256 _birthDate,
    string memory _gender,
    string memory _hairColor,
    string memory _eyeColor,
    string memory _nationality,
    string memory _name2,
    uint256 _birthDate2,
    string memory _gender2,
    string memory _hairColor2,
    string memory _eyeColor2,
    string memory _nationality2
    ) external {
        require(msg.sender == contractOwner, "Only contract owner can add marriages");
        marriages[tokenId].push(Marry(_name, _birthDate, _gender, _hairColor, _eyeColor, _nationality,_name2, _birthDate2, _gender2, _hairColor2, _eyeColor2, _nationality2));
    }

    function getMarry(uint256 tokenId) external view returns (Marry[] memory) {
        return marriages[tokenId];
    }

    function ownerOf(uint256 tokenId) public view override returns (address) {
    return tokenOwners[tokenId];
    }

}
