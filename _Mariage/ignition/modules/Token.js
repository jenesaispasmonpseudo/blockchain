const { buildModule } = require("@nomicfoundation/hardhat-ignition/modules");

const MarriageNFTModule = buildModule("MarriageNFTModule", (m) => {

  const MarriageNFT = m.contract("MarriageNFT");

  return { MarriageNFT };
});

module.exports = MarriageNFTModule;

