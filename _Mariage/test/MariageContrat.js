// Importation des modules nécessaires
const { expect } = require("chai");
const { ethers } = require("hardhat");

// Définition du contrat
describe("MarriageNFT", function() {
  it("Should mint a new token", async function() {
    // Obtention des instances du contrat
    const MarriageNFT = await ethers.getContractFactory("MarriageNFT");
    const [owner, user] = await ethers.getSigners();

    // Déploiement du contrat
    const marriageNFT = await MarriageNFT.deploy();

    // Appel de la fonction mint
    await marriageNFT.connect(owner).mint(user.address);

    // Vérification du nombre total de jetons
    const totalSupply = await marriageNFT.totalSupply();
    expect(totalSupply).to.equal(1);

    // Vérification du propriétaire du jeton
    const ownerOfToken = await marriageNFT.ownerOf(0);
    expect(ownerOfToken).to.equal(user.address);
  });

  it("Should not allow non-contract owner to mint", async function() {
    // Obtention des instances du contrat
    const MarriageNFT = await ethers.getContractFactory("MarriageNFT");
    const [owner, user] = await ethers.getSigners();

    // Déploiement du contrat
    const marriageNFT = await MarriageNFT.deploy();

    // Tentative d'appel de la fonction mint par un utilisateur non autorisé
    await expect(
      marriageNFT.connect(user).mint(user.address)
    ).to.be.revertedWith("Only contract owner can mint");
  });

  it("Should add a child to the marriage NFT", async function() {
    // Obtention des instances du contrat
    const MarriageNFT = await ethers.getContractFactory("MarriageNFT");
    const [owner, user] = await ethers.getSigners();

    // Déploiement du contrat
    const marriageNFT = await MarriageNFT.deploy();

    // Appel de la fonction mint pour créer un NFT de mariage
    await marriageNFT.connect(owner).mint(user.address);

    // Appel de la fonction pour ajouter un enfant au NFT de mariage
    const childName = "Alice";
    const birthDate = Date.now(); 
    const gender = "Femme";
    const hairColor = "Brown"; // Modifié : ajout du mot-clé const
    const eyeColor = "Blue"; // Modifié : ajout du mot-clé const
    const nationality = "Francais"; // Modifié : ajout du mot-clé const
    await marriageNFT.connect(owner).addChild(0, childName, birthDate, gender, hairColor, eyeColor, nationality);

    // Vérification que l'enfant a été ajouté correctement
    const children = await marriageNFT.getChildren(0);
    expect(children.length).to.equal(1);
    expect(children[0].name).to.equal(childName);

    console.log("Child added:", children[0]);
  });

    it("Should add a marriage and retrieve marriage details", async function() {
      // Obtention des instances du contrat
      const MarriageNFT = await ethers.getContractFactory("MarriageNFT");
      const [owner, user] = await ethers.getSigners();
    
      // Déploiement du contrat
      const marriageNFT = await MarriageNFT.deploy();
    
      // Appel de la fonction mint pour créer un NFT de mariage
      await marriageNFT.connect(owner).mint(user.address);
    
      // Appel de la fonction pour ajouter un mariage
      const marriageDetails = {
        name: "John",
        birthDate: 961097600,
        gender: "Male",
        hairColor: "Brun",
        eyeColor: "Bleu",
        nationality: "Americain",
        name2: "John",
        birthDate2: 961097600,
        gender2: "Male",
        hairColor2: "Brun",
        eyeColor2: "Bleu",
        nationality2: "Americain",
      };
      await marriageNFT.connect(owner).addMarry(0, ...Object.values(marriageDetails));
    
      // Vérification que le mariage a été ajouté correctement
      const marriage = await marriageNFT.getMarry(0);
      expect(marriage.name).to.equal(marriageDetails.name);
      expect(marriage.birthDate).to.equal(marriageDetails.birthDate);
      expect(marriage.gender).to.equal(marriageDetails.gender);
      expect(marriage.hairColor).to.equal(marriageDetails.hairColor);
      expect(marriage.eyeColor).to.equal(marriageDetails.eyeColor);
      expect(marriage.nationality).to.equal(marriageDetails.nationality);
      expect(marriage.name2).to.equal(marriageDetails.name2);
      expect(marriage.birthDate2).to.equal(marriageDetails.birthDate2);
      expect(marriage.gender2).to.equal(marriageDetails.gender2);
      expect(marriage.hairColor2).to.equal(marriageDetails.hairColor2);
      expect(marriage.eyeColor2).to.equal(marriageDetails.eyeColor2);
      expect(marriage.nationality2).to.equal(marriageDetails.nationality2);
    
      console.log("Marriage added:", marriage);

    });
});
